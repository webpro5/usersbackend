import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductsDto } from './dto/create-products.dto';
import { UpdateProductsDto } from './dto/update-products.dto';
import { Products } from './entities/products.entity';

let products: Products[] = [
  { id: 1, name: 'Water', price: 100 },
  { id: 2, name: 'Soda', price: 200 },
  { id: 3, name: 'Coke', price: 300 },
];
let lastProductsId = 4;

@Injectable()
export class ProductsService {
  create(createProductsDto: CreateProductsDto) {
    const newProducts: Products = {
      id: lastProductsId++,
      ...createProductsDto, // name,price
    };
    products.push(newProducts);
    return newProducts;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductsDto: UpdateProductsDto) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('user' + JSON.stringify(users[index]));
    // console.log('update' + JSON.stringify(updateUserDto));
    const updateProducts: Products = {
      ...products[index],
      ...updateProductsDto,
    };
    products[index] = updateProducts;
    return updateProducts;
  }

  remove(id: number) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProducts = products[index];
    products.splice(index, 1);

    return deletedProducts;
  }
  reset() {
    products = [
      { id: 1, name: 'Water', price: 100 },
      { id: 2, name: 'Soda', price: 200 },
      { id: 3, name: 'Coke', price: 300 },
    ];
    lastProductsId = 4;
    return 'RESET';
  }
}
